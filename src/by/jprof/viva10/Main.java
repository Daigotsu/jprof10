package by.jprof.viva10;

import java.util.*;
import java.util.stream.Stream;

public class Main {

    private static Cases viva = new Cases();

    public static void main(String[] args) {
        //System.out.println(viva.case1());
        //System.out.println(viva.case2());
        //System.out.println(viva.case3());
        //System.out.println(viva.case4());
        //viva.case5();
        //viva.case6();
        //System.out.println(viva.case7());

        //viva.case8();
        System.out.println(viva.case9());
        //System.out.println(viva.case10());


        //viva.case666();
    }


}

class Cases {
    /**
     * 1. вернёт Doom
     * 2. вернёт Catch The Fire! *
     * 3. выкенет unchecked exception: RuntimeException
     * 4. Почему то не скомпилируется
     */
    public String case1() {
        String case1Value = "Doom";

        try {
            throw new RuntimeException("RuntimeException!");
        } catch (Exception e) {
            case1Value = "Catch The Fire!";
        }

        return case1Value;
    }


    /**
     * Подвох в том что до 128 будет true в дальнейшем будет false т.к. сравниваем ссылку на объекты.
     * <p>
     * 1. true *
     * 2. false
     */
    public Boolean case2() {
        Integer whiskey = 50;
        Integer cola = 30;
        Integer cocktail = whiskey + cola;

        Integer verify = 80;

        return verify == cocktail;
    }

    /**
     * Метод уйдёт в бесконечный цикл, т.к. после первого прохода foundOdd == true
     * и it.next() не будет вызыван.
     * <p>
     * Лучше сделать вообще без вариантов ответов
     * 1. true
     * 2. false
     * 3. что то другое *
     */
    public Boolean case3() {
        List<Integer> list = Arrays.asList(-2, -1, 0, 1, 2);
        Boolean foundOdd = false;

        for (Iterator<Integer> it = list.iterator(); it.hasNext(); )
            foundOdd = foundOdd || isOdd(it.next());

        return foundOdd;
    }

    private static boolean isOdd(int i) {
        return (i & 1) != 0;
    }


    /**
     * Всё просто, но с прицелом что бы спутать. Вывод будет что то типа Wrap@12345 & Wrap@6789
     * <p>
     * 1. Josh & Bloch
     * 2. Bloch & Bloch
     * 3. Ничего из этого *
     * 4. Josh & Josh
     */
    class Wrap {
        String name;
    }

    public String case4() {
        Wrap w1 = new Wrap();
        Wrap w2 = new Wrap();

        w1.name = w2.name = "Josh";
        messup(w1, w2);

        return w1 + " & " + w2;
    }

    private static void messup(Wrap... wraps) {
        wraps[0] = wraps[1];
        wraps[1].name = "Bloch";
    }


    /**
     * Здесь пример как писать не надо, хотя и очень хочется. Дело в том что создаётся анонимный класс и
     * вторые скобки объявлят блок инициализации. Однако переменная при обращении к ней ещё не проинициализировалось, потому NPE
     *
     * 1. WTF?!
     * 2. Hello World
     * 3. NPE *
     * 4. Won't compile
     */
    class SuperMap {
        private final HashMap<String, String> map2 = new HashMap(666, 0.3f) {{
            put("1", "Hello");
            put("2", "World");
            System.out.println(map2.values());
        }};
    }

    public void case5() {
        SuperMap superMap = new SuperMap();
    }


    /**
     * Утащил у Баруха с Puzzlers. С листом всё будет хорошо.
     *
     * 1. соя|тофу
     * 2. ConcurrentModificationException
     * 3. соя|тофу|ConcurrentModificationException
     * 4. соя|тофу|нут *
     */
    public void case6() {
        List<String> vegmenu = new ArrayList<>();
        vegmenu.add("соя");
        vegmenu.add("тофу");
        Stream<String> stream = vegmenu.stream();
        vegmenu.add("нут");
        stream.forEach(System.out::print);
    }


    /**
     * Опять про CME. Какой код выбросит ConcurrentModificationException?
     * Первые два, т.к. в for each тоже неявно используется итератор. В третьем случае ничего страшного не происходит.
     *
     * 1. #1
     * 2. Все три
     * 3. #1, #2 *
     * 4. Исключения не будет
     */
    public String case7() {
        Collection<String> list = new ArrayList<>();
        list.add("Clojure");
        list.add("Ceylon");
        list.add("Groovy");

        // Ouch #1
        Iterator<String> itr = list.iterator();
        while (itr.hasNext()) {
            String lang = itr.next();
            list.remove(lang);
        }

        // Ouch #2
        for (String language : list) {
            list.remove(language);
        }

        // Ouch #3
        list.stream().map(list::remove);

        return list.toString();
    }

    /**
     * Опять утащил у Баруха, но они очень хороши, пазлеры эти.
     * Будет NPE во втором случае, т.к. Optional.map(null)  сам по себе выкенет NPE при вызове и второй orElse
     * не будет вызван. *это написано в javadoc map*
     *
     * 1. Java|Professionals
     * 2. NPE|Professionals
     * 3. NPE|NPE
     * 4. Java|NPE *
     */
    public void case8() {
        System.out.println(Optional.of("Java").orElse(null));
        System.out.println(Optional.empty().map(null).orElse("Professionals"));
    }

    /**
     * Как найти максимум? Весь интерес в том что max принимает компоратор, хотя на самом деле компоратор - это функция которая возвращает число
     * вот иMath::max возвращает само число максимальное.
     *
     * В итоге:
     * Math.max(-100, -50) = -50 -> -50 < 0 значит берём -50
     * Math.max(-50, 0) = 0 -> значит -50 == 0 для Comparator
     * и так далее, где -50 будет всегда оставаться как бы большим числом
     *
     * 1. -100
     * 2. -50 *
     * 3. 0
     * 4. 100
     *
     */
    public Integer case9() {
        return Stream.of(-100, -50, 0, 50, 100).max(Math::max).get();
    }

    public void case10() {
        // TODO
    }



    /**
     * Просто интересный кейс на, а вдруг кто угадает или знает
     * вовод: 0 1 2 3 4 5 6 7 8 9
     */
    public void case666() {
        Random random = new Random(-6732303926L);
        for (int i = 0; i < 10; i++) {
            System.out.print(random.nextInt(10) + " ");
        }
    }

    /**
     * Указать количество примитивов в java. 8
     */
    public void case999() {
        int i;
        long l;
        byte b;
        double d;
        boolean t;
        short s;
        float f;
        char c;
    }
}